import React from 'react';
import { View, ScrollView, KeyboardAvoidingView, Alert, TextInput, Button, TouchableOpacity, Text } from 'react-native';
// import Mytextinput from './components/Mytextinput';
// import Mybutton from './components/Mybutton';
import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

export default class RegisterUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_name: '',
            user_contact: '',
            user_address: '',
            input_user_id: '1',

        };
    }
    showUser = () => {
        db.transaction(tx => {
            tx.executeSql(
                'SELECT * FROM table_user where user_id = ?',
                [1],
                (tx, results) => {
                    let len = results.rows.length;
                    console.log('len', len);
                    if (len > 0) {
                        Alert.alert(
                            'user showed',
                            `${results.rows.item(0).user_contact}`,
                            [
                                {
                                    text: 'Ok',
                                    onPress: () => null,
                                },
                            ],
                            { cancelable: false }
                        );
                        console.log(results.rows.item(0).user_contact);
                    }
                    else {
                        Alert.alert(
                            'failed',
                            'no user to show',
                            [
                                {
                                    text: 'Ok',
                                    onPress: () => null,
                                },
                            ],
                            { cancelable: false }
                        );
                    }
                }
            )
        })
    }
    register_user = () => {
        var that = this;
        const { user_name } = this.state;
        const { user_contact } = this.state;
        const { user_address } = this.state;
        //alert(user_name, user_contact, user_address);
        if (user_name) {
            if (user_contact) {
                if (user_address) {
                    db.transaction(function (tx) {
                        tx.executeSql(
                            'INSERT INTO table_user (user_name, user_contact, user_address) VALUES (?,?,?)',
                            [user_name, user_contact, user_address],
                            (tx, results) => {
                                console.log('Results', results.rowsAffected);
                                if (results.rowsAffected > 0) {
                                    Alert.alert(
                                        'Success',
                                        'You are Registered Successfully',
                                        [
                                            {
                                                text: 'Ok',
                                                onPress: () => null,
                                            },
                                        ],
                                        { cancelable: false }
                                    );
                                } else {
                                    alert('Registration Failed');
                                }
                            }
                        );
                    });
                } else {
                    alert('Please fill Address');
                }
            } else {
                alert('Please fill Contact Number');
            }
        } else {
            alert('Please fill Name');
        }
    };
    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <KeyboardAvoidingView
                        behavior="padding"
                        style={{ flex: 1, justifyContent: 'space-between' }}>
                        <TextInput
                            placeholder="Enter Name"
                            onChangeText={user_name => this.setState({ user_name })}
                            style={{ padding: 10 }}
                            underlineColorAndroid="transparent"
                            placeholderTextColor="#007FFF"
                            blurOnSubmit={false}
                        />
                        <TextInput
                            placeholder="Enter Contact No"
                            onChangeText={user_contact => this.setState({ user_contact })}
                            maxLength={10}
                            keyboardType="numeric"
                            style={{ padding: 10 }}
                            underlineColorAndroid="transparent"
                            placeholderTextColor="#007FFF"
                            blurOnSubmit={false}
                        />
                        <TextInput
                            placeholder="Enter Address"
                            onChangeText={user_address => this.setState({ user_address })}
                            maxLength={225}
                            numberOfLines={5}
                            multiline={true}
                            style={{ textAlignVertical: 'top', padding: 10 }}
                            underlineColorAndroid="transparent"
                            placeholderTextColor="#007FFF"
                            blurOnSubmit={false}
                        />
                        <TouchableOpacity style={{ alignItems: 'center', backgroundColor: '#deb887', color: '#ffffff', padding: 10, marginTop: 16, marginLeft: 35, marginRight: 35 }} onPress={this.register_user.bind(this)}>
                            <Text style={{ color: '#ffffff' }}>Submit</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ alignItems: 'center', backgroundColor: '#0081AB', color: '#ffffff', padding: 10, marginTop: 16, marginLeft: 35, marginRight: 35 }} onPress={() => this.showUser()}>
                            <Text style={{ color: '#ffffff' }}>Show</Text>
                        </TouchableOpacity>

                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        );
    }
}
