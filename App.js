import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';
// import LottieView from 'lottie-react-native';
// import SQLite from 'react-native-sqlite-storage';
// import RegisterUser from './RegisterUser';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import First from './src/First';
import Second from './src/Second';
import Third from './src/Third';
import SettingsScreen from './src/SettingScreen';
import Welcome from './src/Welcome';
import store from './src/store/store';
import { Provider } from 'react-redux';


const Navigator = createAppContainer(createBottomTabNavigator({
  Splash: Welcome,
  Screen1: First,
  Screen2: Second,
  Screen3: Third,
  Settings: SettingsScreen,
}));

// db = SQLite.openDatabase({ name: "UserDatabase.db" })

export default class App extends Component {
  //SQLite DB 

  // constructor(props) {
  //   super(props);
  //   db.transaction(function (txn) {
  //     txn.executeSql(
  //       "SELECT name FROM sqlite_master WHERE type='table' AND name='table_user'",
  //       [],
  //       function (tx, res) {
  //         console.log('item:', res.rows.length);
  //         if (res.rows.length == 0) {
  //           txn.executeSql('DROP TABLE IF EXISTS table_user', []);
  //           txn.executeSql(
  //             'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
  //             []
  //           );
  //         }
  //       }
  //     );
  //   });
  // }

  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  }
});
