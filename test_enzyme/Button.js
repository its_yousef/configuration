import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const Button = (props) => {
    return (
        <View>
            <TouchableOpacity onPress={props.onPress}>
                <Text style={{ fontSize: 30 }}>{props.text}</Text>
            </TouchableOpacity>
        </View>
    );
}

export { Button };