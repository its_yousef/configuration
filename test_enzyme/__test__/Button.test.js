import 'react-native';
import React from 'react';
import { Button } from '../Button';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// import { expect } from 'chai';

configure({ adapter: new Adapter() });

describe('Button', () => {
  let wrapper;
  let props;
  beforeEach(() => {
    props = { text: 'hello', onPress: jest.fn() }
    wrapper = shallow(< Button {...props} />);
  })

  it('should render right', () => {
    expect(wrapper.find('TouchableOpacity')).toHaveLength(1);
  });

  it('should place the hello word inside the Text field', () => {
    expect(wrapper.find('Text').contains('hello')).toBe(true);
  });

  it('should call onClickHandler', () => {
    wrapper.find('TouchableOpacity').prop('onPress')();
    expect(props.onPress).toHaveBeenCalledTimes(1);
  })
});