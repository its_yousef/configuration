import { createContext } from 'react';
// import { LANG_ENGLISH } from './constants';

// First function argument represents default value
const { Provider, Consumer } = createContext(ENGLISH);

export const LanguageProvider = Provider;
export const LanguageConsumer = Consumer;