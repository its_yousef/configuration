// import { AsyncStorage } from 'react-native';
import { CHANGE_LANGUAGE, CHANGE_TEXTALIGN } from './types';

export const changeLanguage = selectedLanguage => {
    return {
        payload: selectedLanguage,
        type: CHANGE_LANGUAGE
    }
}

//-------------------------------------------------------------
export const changeTextAlign = selectedStyle => {
    return {
        payload: selectedStyle,
        type: CHANGE_TEXTALIGN
    }
}