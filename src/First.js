import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { changeTextAlign } from './actions';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;


class First extends Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 25, width: SCREEN_WIDTH, textAlign: this.props.textAlign, backgroundColor: 'grey' }}>
                    {this.props.languageFile.welcome}
                </Text>
            </View>
        )
    }
}

function mapStateToProps({ changeLanguage, changeTextAlign }) {
    return {
        languageFile: changeLanguage,
        textAlign: changeTextAlign
    }
}

export default connect(mapStateToProps)(First);