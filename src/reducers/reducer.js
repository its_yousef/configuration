import Configuration from '../configuration.json';
import { combineReducers } from 'redux';
import { CHANGE_LANGUAGE, CHANGE_TEXTALIGN } from '../actions/types';

const LANGUAGE_INITIAL_STATE = Configuration.language.PERSIAN;


const changeLanguage = (state = LANGUAGE_INITIAL_STATE, action) => {
    console.log("ACTION TYPE !", action.type);
    console.log("ACTION PAYLOAD !", action.payload);
    // console.log("CHANGE_LANGUAGE", CHANGE_LANGUAGE)
    switch (action.type) {
        case CHANGE_LANGUAGE:
            return Configuration.language[action.payload];
        default:
            // console.log("zabanooo default", Configuration.language.action.payload)
            return state;
    }
}

//----------------------------------------------------------------------
const TEXTALIGN_INITIAL_STATE = Configuration.textStyle.LTR;


const changeTextAlign = (state = TEXTALIGN_INITIAL_STATE, action) => {
    console.log("ACTION TYPE !", action.type);
    console.log("ACTION PAYLOAD !", action.payload);

    switch (action.type) {
        case CHANGE_TEXTALIGN:
            return Configuration.textStyle[action.payload];
        default:
            return state;
    }
    
}




export default combineReducers({ changeLanguage, changeTextAlign });