import React, { Component } from 'react';
import { View, Text, ActivityIndicator, AsyncStorage, ToastAndroid } from 'react-native';
import * as actions from './actions';
import { connect } from 'react-redux';


class Welcome extends Component {
    state = {
        loading: 'true'
    }

    componentDidMount() {
        this.checkLanguage()
        this.checkTextAlign()
        setTimeout(() => this.props.navigation.navigate("Screen1"), 2000)


    }

    checkLanguage = async () => {
        // console.log("in checkLanguage")
        let selectedLanguage = await AsyncStorage.getItem("language");
        // console.log("ASYNC LANGUAGE", selectedLanguage)

        if (selectedLanguage != null) {
            this.props.changeLanguage(selectedLanguage);
            // this.props.dispatch({ type: "changeLanguage", payload: "PERSIAN" });
        } else {
            this.props.changeLanguage("ENGLISH");
        }


    }

    checkTextAlign = async () => {
        let selectedTextAlign = await AsyncStorage.getItem("textAlign");
        
        if (selectedTextAlign != null) {
            this.props.changeTextAlign(selectedTextAlign);
        } else {
            this.props.changeTextAlign("LTR")
        }
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size={150} color="black" />
            </View>
        )
    }
}




export default connect(null, actions)(Welcome);