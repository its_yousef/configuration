import React, { Component } from 'react';
import { View, Text, Picker, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import * as actions from './actions';

class SettingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedLanguage: "ENGLISH",
            selectedTextStyle: "LTR"
        }
    }

    componentWillMount() {
        this.checkLanguage();
        this.checkTextAlign();
    }

    checkLanguage = async () => {
        let savedLanguage = await AsyncStorage.getItem("language");
        // console.log('savedLanguage', savedLanguage);

        if (savedLanguage != null) {
            this.setState({ selectedLanguage: savedLanguage })
        } else {
            this.setState({ selectedLanguage: "ENGLISH" })
        }
    }

    checkTextAlign = async () => {
        let savedTextAlign = await AsyncStorage.getItem("textAlign");
        // console.log('savedLanguage', savedLanguage);

        if (savedTextAlign != null) {
            this.setState({ selectedTextStyle: savedTextAlign })
        } else {
            this.setState({ selectedTextStyle: "LTR" })
        }
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ marginBottom: 100, fontSize: 25 }}>
                    settinge koskesh
                </Text>

                <Picker
                    selectedValue={this.state.selectedLanguage}
                    style={{ height: 50, width: 100, marginBottom: 50 }}
                    onValueChange={async (itemValue) => {
                        await this.setState({ selectedLanguage: itemValue })
                        await this.props.changeLanguage(this.state.selectedLanguage)
                        // console.log("state selectedlang 2", this.state.selectedLanguage)
                        await AsyncStorage.setItem("language", this.state.selectedLanguage)
                    }}>
                    <Picker.Item label="persian" value="PERSIAN" />
                    <Picker.Item label="english" value="ENGLISH" />
                </Picker>

                <Picker
                    selectedValue={this.state.selectedTextStyle}
                    style={{ height: 50, width: 100 }}
                    onValueChange={async (itemValue) => {

                        await this.setState({ selectedTextStyle: itemValue })
                        await this.props.changeTextAlign(this.state.selectedTextStyle)
                        await AsyncStorage.setItem("textAlign", this.state.selectedTextStyle)

                    }}>
                    <Picker.Item label="RTL" value="RTL" />
                    <Picker.Item label="LTR" value="LTR" />
                </Picker>
            </View>
        )
    }
}

export default connect(null, actions)(SettingScreen);